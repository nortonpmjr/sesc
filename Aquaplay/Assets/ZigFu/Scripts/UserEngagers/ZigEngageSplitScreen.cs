using UnityEngine;
using System.Collections;

public class ZigEngageSplitScreen : MonoBehaviour {
    public GameObject LeftPlayer;
    public GameObject RightPlayer;

	public GameController gameController;

    public ZigTrackedUser leftTrackedUser { get; private set; }
    public ZigTrackedUser rightTrackedUser { get; private set; }

    Bounds leftRegion = new Bounds(new Vector3(-500, 0, -3000), new Vector3(1000, 3000, 3500));
    Bounds rightRegion = new Bounds(new Vector3(500, 0, 3000), new Vector3(1000, 3000, 3500));

    bool AllUsersEngaged { get { return null != leftTrackedUser && null != rightTrackedUser; } }

    ZigTrackedUser LookForTrackedUserInRegion(ZigInput zig, Bounds region) {
        foreach (ZigTrackedUser trackedUser in zig.TrackedUsers.Values) {
            if (trackedUser.SkeletonTracked && region.Contains(trackedUser.Position) && trackedUser != leftTrackedUser && trackedUser != rightTrackedUser) {
                return trackedUser;
            }
        }
        return null;
    }

    void Zig_Update(ZigInput zig) {
        bool areTheyEngaged = AllUsersEngaged;
        // blue user
        if (null == leftTrackedUser) {
            leftTrackedUser = LookForTrackedUserInRegion(zig, leftRegion);
            if (null != leftTrackedUser) {
                leftTrackedUser.AddListener(LeftPlayer);
                SendMessage("UserEngagedLeft", this, SendMessageOptions.DontRequireReceiver);
				StartCoroutine("DisengageBlue");
				//gameController.setAzul = true;
				//gameController.setVermelho = true;
                gameController.reconhecerJogador = true;
            }
        }
        // right user
        if (null == rightTrackedUser) {
            rightTrackedUser = LookForTrackedUserInRegion(zig, rightRegion);
            if (null != rightTrackedUser) {
                rightTrackedUser.AddListener(RightPlayer);
                SendMessage("UserEngagedRight", this, SendMessageOptions.DontRequireReceiver);
                StartCoroutine("DisengageRed");
                //gameController.setAzul = true;
                //gameController.setVermelho = true;
                gameController.reconhecerJogador = true;
            }
        }
        if (!areTheyEngaged && AllUsersEngaged) {
            SendMessage("AllUsersEngaged", this, SendMessageOptions.DontRequireReceiver);
        }
    }

	IEnumerator DisengageBlue()
	{
		yield return new WaitForSeconds(5f);
        if (leftTrackedUser == null)
            //gameController.setAzul = true;
            gameController.reconhecerJogador = true;
	}

	IEnumerator DisengageRed()
	{
		yield return new WaitForSeconds(5f);
        if (rightTrackedUser == null)
            //gameController.setVermelho = true;
            gameController.reconhecerJogador = true;
	}

    void Zig_UserLost(ZigTrackedUser user) {
        if (user == leftTrackedUser) {
            leftTrackedUser = null;
            SendMessage("UserDisengagedLeft", this, SendMessageOptions.DontRequireReceiver);
			StartCoroutine("DisengageBlue");
        }
        if (user == rightTrackedUser) {
            rightTrackedUser = null;
            SendMessage("UserDisengagedRight", this, SendMessageOptions.DontRequireReceiver);
			StartCoroutine("DisengageRed");
        }
    }
}

