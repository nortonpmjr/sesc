﻿#define JOGADOR_UNICO

using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

    public int bluePoints, redPoints;
    public float time;
    private float _time;

    public AudioClip moveCamera, beginGame, pointSign, victorySign, cheering, gameMusic, outMusic;
    private AudioSource audioSource;

    private ComponentTracker tracker;

#if JOGADOR_UNICO
    private bool jogadorReconhecido;
    public bool JogadorReconhecido 
    {
        get { return jogadorReconhecido; }
        set
        {
            if (jogadorReconhecido != value) PlaySound(pointSign);
            jogadorReconhecido = value;
        }
    }

    public bool reconhecerJogador;
#else
    private bool azulReconhecido, vermelhoReconhecido;
    public bool AzulReconhecido
    {
        get { return azulReconhecido; }
        set
        {
            if (azulReconhecido != value)
            {
                tracker.GetObject(value ? "AzulReconhecido" : "AzulNaoReconhecido").SetActive(true);
                tracker.GetObject(value ? "AzulNaoReconhecido" : "AzulReconhecido").SetActive(false);
                tracker.GetElement<PlacarAnimation>(value ? "AzulReconhecido" : "AzulNaoReconhecido").PlayAnimation();
                PlaySound(pointSign);
            }
            azulReconhecido = value;
        }
    }
    public bool VermelhoReconhecido
    {
        get { return vermelhoReconhecido; }
        set
        {
            if (vermelhoReconhecido != value)
            {
                tracker.GetObject(value ? "VermelhoReconhecido" : "VermelhoNaoReconhecido").SetActive(true);
                tracker.GetObject(value ? "VermelhoNaoReconhecido" : "VermelhoReconhecido").SetActive(false);
                tracker.GetElement<PlacarAnimation>(value ? "VermelhoReconhecido" : "VermelhoNaoReconhecido").PlayAnimation();
                PlaySound(pointSign);
            }
            vermelhoReconhecido = value;
        }
    }

    public bool setAzul, setVermelho;
#endif

    public float tempoReconhecimento;
    private float _tempoReconhecimento;

    public float tempoPlacar;
    private float _tempoPlacar;

    private bool pronto;

    public float cameraTimeSpeed;

    public int state = 1;
    // 0 = tela inicial
    // 1 = reconhecer jogadores
    // 2 = jogo
    // 3 = placar

    public Transform[] balls;
    private Vector3[] ballsInitialPosition;

    private void Start()
    {
        tracker = ComponentTracker.Instance;

#if !JOGADOR_UNICO
        tracker.GetObject("VermelhoReconhecido").SetActive(false);
        tracker.GetObject("AzulReconhecido").SetActive(false);
#endif
        tracker.GetObject("Texto_JogadoresIdentificados").SetActive(false);
        audioSource = tracker.GetElement<AudioSource>("AudioSource");

        ballsInitialPosition = new Vector3[balls.Length];
        for (int i = 0; i < balls.Length; i++) ballsInitialPosition[i] = balls[i].transform.position;
    }

    private void Update()
    {
#if JOGADOR_UNICO
        if(reconhecerJogador)
        {
            reconhecerJogador = false;
            JogadorReconhecido = !JogadorReconhecido;
        }
#else
        if (setAzul)
        {
            setAzul = false;
            AzulReconhecido = !AzulReconhecido;
        }

        if (setVermelho)
        {
            setVermelho = false;
            VermelhoReconhecido = !VermelhoReconhecido;
        }
#endif   

        if (Input.GetKeyDown(KeyCode.R)) Application.LoadLevel(Application.loadedLevel);

        switch (state) // HC State Machine
        {
            case 0: // Tela Inicial
                break;
            case 1: // Reconhecer Jogadores
#if JOGADOR_UNICO
                if(JogadorReconhecido)
#else
                if (vermelhoReconhecido && azulReconhecido)
#endif
                {
                    if (!pronto)
                    {
                        tracker.GetObject("Texto_IdentificandoJogadores").SetActive(false);
                        tracker.GetObject("Texto_JogadoresIdentificados").SetActive(true);
                    }

                    pronto = true;
                    _tempoReconhecimento = Mathf.Clamp(_tempoReconhecimento - Time.deltaTime, 0f, _tempoReconhecimento);
                    tracker.GetElement<TextMesh>("JogadoresIdentificados_Contagem").text = Mathf.FloorToInt(_tempoReconhecimento).ToString();

                    if (_tempoReconhecimento <= 0f) // Transição
                    {
                        _time = time;
                        bluePoints = redPoints = 0;
                        tracker.GetObject("PonteiroAzul").transform.localEulerAngles = Vector3.zero;
                        tracker.GetObject("PonteiroLaranja").transform.localEulerAngles = Vector3.zero;
                        PlaySound(beginGame, 0.5f);
                        
                        //audioSource.Stop();
                        audioSource.clip = gameMusic;
                        audioSource.Play();

                        state++;
                        CameraDown(2f);
                    }
                }
                else
                {
                    if (pronto)
                    {
                        tracker.GetObject("Texto_JogadoresIdentificados").SetActive(false);
                        tracker.GetObject("Texto_IdentificandoJogadores").SetActive(true);
                    }

                    pronto = false;
                    _tempoReconhecimento = tempoReconhecimento + 1f;
                }
                break;
            case 2: // Jogo
                tracker.GetElement<TextMesh>("PlacarAzul_Text").text = string.Format("{0:00}", bluePoints);
                tracker.GetElement<TextMesh>("PlacarLaranja_Text").text = string.Format("{0:00}", redPoints);
                _time -= Time.deltaTime;
                tracker.GetElement<TextMesh>("Time_Text").text = string.Format("{0}:{1:00}", (int)(_time / 60f), (int)(_time % 60f));

                if (_time <= 0f || redPoints >= 20 || bluePoints >= 20) // Transição
                {
                    state++;
                    CameraDown(-18f);

                    foreach (Reposition r in FindObjectsOfType<Reposition>()) r.RepositionObject();

                    tracker.GetElement<TextMesh>("PlacarFinal_Azul").text = string.Format("{0:00}", bluePoints);
                    tracker.GetElement<TextMesh>("PlacarFinal_Vermelho").text = string.Format("{0:00}", redPoints);

                    string barra = (redPoints > bluePoints) ? "VermelhoVenceu_bg" : "AzulVenceu_bg";
                    string timex = (redPoints > bluePoints) ? "VermelhoVenceu_TimeVemelho" : "AzulVenceu_TimeAzul";
                    string venceu = (redPoints > bluePoints) ? "VermelhoVenceu_Venceu" : "AzulVenceu_Venceu";

                    iTween.ScaleTo(tracker.GetObject(barra), iTween.Hash(
                            "y", 11.92841f,
                            "time", 1f,
                            "easetype", iTween.EaseType.easeOutCirc
                        ));

                    iTween.MoveTo(tracker.GetObject(timex), iTween.Hash(
                            "x", 8.880021f,
                            "time", 1f,
                            "easetype", iTween.EaseType.easeOutCirc,
                            "delay", 0.8f,
                            "islocal", true
                        ));

                    iTween.MoveTo(tracker.GetObject(venceu), iTween.Hash(
                            "x", 8.880021f,
                            "time", 1f,
                            "easetype", iTween.EaseType.easeOutCirc,
                            "delay", 1f,
                            "islocal", true
                        ));

                    iTween.MoveTo(tracker.GetObject("Venceu_Player"), iTween.Hash(
                            "x", -7.35f,
                            "time", 1f,
                            "easetype", iTween.EaseType.easeOutCirc,
                            "delay", 1.3f
                        ));

                    _tempoPlacar = tempoPlacar;

                    PlaySound(victorySign);
                    PlaySound(cheering);

                    audioSource.Stop();
                    audioSource.clip = outMusic;
                    audioSource.Play();

                    for (int i = 0; i < balls.Length; i++)
                    {
                        balls[i].transform.position = ballsInitialPosition[i];
                        balls[i].rigidbody.velocity = Vector3.zero;
                    }
                }
#if !JOGADOR_UNICO
				if(!azulReconhecido || !vermelhoReconhecido)
				{
					_tempoReconhecimento = tempoReconhecimento;
					AzulReconhecido = VermelhoReconhecido = false;
					state = 1;
					CameraRestart();
				}
#endif
                break;
            case 3: // Placar Final
                _tempoPlacar -= Time.deltaTime;
                if (_tempoPlacar <= 0f)
                {
#if JOGADOR_UNICO
                    jogadorReconhecido = false;
#else
                    azulReconhecido = vermelhoReconhecido = false;
#endif
                    _tempoReconhecimento = tempoReconhecimento;

                    state = 1;
                    CameraRestart();
                }
                break;
            default: break;
        }
    }

    public void BluePoint()
    {
        bluePoints += 2;
        tracker.GetElement<PlacarAnimation>("PlacarAzul").PlayAnimation();
        tracker.GetElement<PointerAnimation>("PonteiroAzul").PlayAnimation(bluePoints);
        PlaySound(pointSign);
    }

    public void RedPoint()
    {
        redPoints += 2;
        tracker.GetElement<PlacarAnimation>("PlacarLaranja").PlayAnimation();
        tracker.GetElement<PointerAnimation>("PonteiroLaranja").PlayAnimation(redPoints);
        PlaySound(pointSign);
    }

    private void CameraDown(float yPos)
    {
        iTween.MoveTo(Camera.main.gameObject, iTween.Hash(
                "y", yPos,
                "time", cameraTimeSpeed,
                "easetype", iTween.EaseType.easeInOutBack
            ));

        PlaySound(moveCamera);
    }

    private void CameraRestart()
    {
        iTween.MoveTo(Camera.main.gameObject, iTween.Hash(
                "y", -28,
                "time", cameraTimeSpeed/2f,
                "easetype", iTween.EaseType.easeInBack,
                "oncomplete", "CameraStart",
                "oncompletetarget", gameObject
            ));

        PlaySound(moveCamera);
    }

    private void CameraStart()
    {
        Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, 32f, Camera.main.transform.position.z);

        iTween.MoveTo(Camera.main.gameObject, iTween.Hash(
                "y", 22,
                "time", cameraTimeSpeed / 2f,
                "easetype", iTween.EaseType.easeOutBack
            ));
    }

    private void PlaySound(AudioClip clip, float delay = 0f)
    {
        StartCoroutine(PlaySoundCoroutine(clip, delay));
    }

    private IEnumerator PlaySoundCoroutine(AudioClip clip, float delay)
    {
        yield return new WaitForSeconds(delay);

        AudioSource.PlayClipAtPoint(clip, audioSource.transform.position);
    }
}