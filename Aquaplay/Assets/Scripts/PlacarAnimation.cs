﻿using UnityEngine;
using System.Collections;

public class PlacarAnimation : MonoBehaviour {

    public float time1, time2, offset;

    public bool play;
    private bool isPlaying;
    private Vector3 localPosition;

    private void Start()
    {
        localPosition = transform.localPosition;
    }

    private void Update()
    {
        if (play)
        {
            play = false;
            PlayAnimation();
        }
    }

    public void PlayAnimation()
    {
        if (isPlaying) return;

        isPlaying = true;

        iTween.MoveTo(gameObject, iTween.Hash(
                "y", transform.localPosition.y + offset,
                "islocal", true,
                "time", time1,
                "easetype", iTween.EaseType.easeOutQuad,
                "oncomplete", "PlayAnimationCallback",
                "oncompletetarget", gameObject
            ));
    }

    private void PlayAnimationCallback()
    {
        iTween.MoveTo(gameObject, iTween.Hash(
                "position", localPosition,
                "islocal", true,
                "time", time2,
                "easetype", iTween.EaseType.easeOutElastic,
                "oncomplete", "StopAnimation",
                "oncompletetarget", gameObject
            ));
    }

    private void StopAnimation()
    {
        isPlaying = false;
    }
}
