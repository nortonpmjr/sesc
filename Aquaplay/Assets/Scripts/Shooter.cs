﻿using UnityEngine;
using System.Collections;

public class Shooter : MonoBehaviour {

    public Bubble bubblePrefab;
    public int bubblesPerShoot = 1;
    public float instantiationDelay;

    public bool shoot;
    public KeyCode testInput;

    public ButtonAnimation button;

    private void Update()
    {
        if (shoot)
        {
            shoot = false;
            Shoot();
        }

        if(Input.GetKeyDown(testInput))
        {
            Shoot();
        }
    }

    public void Shoot()
    {
        if(bubblePrefab && bubblesPerShoot > 0)
        {
            button.PlayAnimation();
            StartCoroutine(IShoot());
        }
    }

    private IEnumerator IShoot()
    { 
        for(int i=0; i<bubblesPerShoot; i++)
        {
            //Bubble b = Instantiate(bubblePrefab, transform.position, transform.rotation) as Bubble;
            Instantiate(bubblePrefab, transform.position, transform.rotation);

            yield return new WaitForSeconds(instantiationDelay);
        }
    }
}
