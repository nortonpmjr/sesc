﻿using UnityEngine;
using System.Collections;

public class Magnet : MonoBehaviour {

    public Transform anchor;
    private Vector3 distance;

    private void Start()
    {
        if (!anchor) Destroy(this);

        distance = transform.position - anchor.position;
        transform.parent = null;
    }

    private void LateUpdate()
    {
        transform.position = anchor.position + distance;
    }

}
