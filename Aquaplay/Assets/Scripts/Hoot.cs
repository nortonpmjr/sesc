﻿using UnityEngine;
using System.Collections;

public class Hoot : MonoBehaviour {

    public GameObject target;
    public string message;


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 9)
        {
            if (target) target.SendMessage(message);
        }
    }
}
