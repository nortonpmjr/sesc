﻿using UnityEngine;
using System.Collections;

public class Reposition : MonoBehaviour {

    private Vector3 _position, _eulerAngles, _scale;

    private void Start()
    {
        _position = transform.position;
        _eulerAngles = transform.eulerAngles;
        _scale = transform.localScale;
    }

    public void RepositionObject()
    {
        transform.position = _position;
        transform.eulerAngles = _eulerAngles;
        transform.localScale = _scale;
    }
}
