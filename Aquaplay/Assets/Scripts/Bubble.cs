﻿using UnityEngine;
using System.Collections;

public class Bubble : MonoBehaviour {

    public float timeToDie = 3f, zRotVariance;
    public Vector2 force, scale;

    private void Start()
    {
        transform.localScale = Vector3.one * Random.Range(scale.x, scale.y);
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, transform.localEulerAngles.z + Random.Range(-zRotVariance, zRotVariance));
        rigidbody.AddForce(transform.up * Random.Range(force.x, force.y));

        Destroy(gameObject, timeToDie);
    }

    private void Update()
    {
        if (rigidbody.velocity.y < 0f) Destroy(gameObject);
    }
}
