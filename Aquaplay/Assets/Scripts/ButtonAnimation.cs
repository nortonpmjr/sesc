﻿using UnityEngine;
using System.Collections;

public class ButtonAnimation : MonoBehaviour {

    public float time = 1f;
    public float offset = 1f;

    private Vector3 localPosition;
    public bool play = false;
    private bool isPlaying = false;

    public AudioClip sound;

    private void Start()
    {
        localPosition = transform.localPosition;
    }

    private void Update()
    {
        if (play)
        {
            play = false;
            PlayAnimation();
        }
    }

    public void PlayAnimation()
    {
        if (isPlaying) return;

        isPlaying = true;

        iTween.MoveTo(gameObject, iTween.Hash(
                "z", offset,
                "islocal", true,
                "time", time,
                "easetype", iTween.EaseType.easeOutQuad,
                "oncomplete", "PlayAnimationCallback",
                "oncompletetarget", gameObject
            ));

        AudioSource.PlayClipAtPoint(sound, ComponentTracker.Instance.GetObject("AudioSource").transform.position);
    }

    private void PlayAnimationCallback()
    {
        iTween.MoveTo(gameObject, iTween.Hash(
                "position", localPosition,
                "islocal", true,
                "time", time,
                "easetype", iTween.EaseType.easeOutBack,
                "oncomplete", "StopAnimation",
                "oncompletetarget", gameObject
            ));
    }

    private void StopAnimation()
    {
        isPlaying = false;
    }
}
