﻿using UnityEngine;
using System.Collections;

public class KinectController : MonoBehaviour {

	public Transform rightHand;
	public Transform leftHand;
	public Transform head;
	public Transform rightShoulder;
	public Transform leftShoulder;

	private bool isMoving;

	public Shooter shooter;
	public Shooter otherShooter;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if((IsAbove(rightShoulder, rightHand) || IsAbove(leftShoulder, leftHand)) && !isMoving)
		{
			isMoving = true;
			CalculateMovementSpeed(rightHand);
			CalculateMovementSpeed(leftHand);
		}
	} 

	void CalculateMovementSpeed(Transform hand)
	{
		StartCoroutine(CalculateMovementSpeedCoroutine(hand));
	}

	IEnumerator CalculateMovementSpeedCoroutine(Transform hand)
	{
		float initialPosition = hand.position.y;
		float finalPosition = 0.0f;
		bool isHandMoving = true;
		float time = 0.0f;
		while(isHandMoving)
		{
			time += Time.deltaTime;
			if(hand == rightHand)
			{
				if(hand.position.y < rightShoulder.position.y)
				{
					isHandMoving = false;
					finalPosition = hand.position.y;
				}
				yield return new WaitForSeconds(0.2f);
			}else if(hand == leftHand)
			{
				if(hand.position.y < leftShoulder.position.y)
				{
					isHandMoving = false;
					finalPosition = hand.position.y;
				}
				yield return new WaitForSeconds(0.2f);
			}

		}
		float speed;
		speed = (initialPosition - finalPosition)/time;
		if(speed > 3.0f && speed < 7.0f)
		{
			shooter.Shoot();
			otherShooter.Shoot();
		}
		else if(speed > 7.1f && speed < 11.0f)
		{
			for(int i = 0; i < 2; i++)
			{
				shooter.Shoot();
				otherShooter.Shoot();
				yield return new WaitForSeconds(0.1f);
			}
		}
		else if (speed > 11.1f)
		{
			for(int i = 0; i < 3; i++)
			{
				shooter.Shoot();
				otherShooter.Shoot();
				yield return new WaitForSeconds(0.1f);
			}
		}

		Debug.Log(string.Format("Speed: {0}", speed));
		isMoving = false;
		yield return 1;
	}

	bool IsAbove(Transform object01, Transform object02)
	{
		return object01.position.y < object02.position.y ? true : false;
	}
}
