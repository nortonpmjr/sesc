﻿using UnityEngine;
using System.Collections;

public class LerpFollow : MonoBehaviour {

    public Transform target;
    public float speed;

    private void Start()
    {
        transform.parent = null;
    }

    private void LateUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, target.position, Time.deltaTime * speed);
        transform.rotation = Quaternion.Lerp(transform.rotation, target.transform.rotation, Time.deltaTime * speed);
    }
}
