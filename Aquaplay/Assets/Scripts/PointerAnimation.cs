﻿using UnityEngine;
using System.Collections;

public class PointerAnimation : MonoBehaviour {

    public float maxRotation, time;
    public int steps;
    public bool play;
    private bool isPlaying;
    private Vector3 localPosition;

    private void Start()
    {
        localPosition = transform.localPosition;
    }

    private void Update()
    {
        if (play)
        {
            play = false;
            PlayAnimation(++steps);
        }
    }

    public void PlayAnimation(float steps)
    {
        if (isPlaying) StopAnimation();

        isPlaying = true;

        iTween.RotateTo(gameObject, iTween.Hash(
                "z", maxRotation / 20f * steps,
                "islocal", true,
                "time", time,
                "easetype", iTween.EaseType.easeOutBack
            ));
    }

    private void StopAnimation()
    {
        if(GetComponent<iTween>()) Destroy(GetComponent<iTween>());
        isPlaying = false;
    }
}
